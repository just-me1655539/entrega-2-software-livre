
import hashlib

class User:                                         #definição da classe User
    def __init__(self, name, passwd, role):         #inicialização dos atributos da classe User
        self.name = name
        self.passwd = passwd
        self.role = role




usr = User                                          #inicialização do objeto usr

usr.role = 'plebeu'                                 #definição da hierarquia

usr.name = input('Insira seu nome de usuário: ')    #pega o nome de usuário

usr.passwd = input('Insira a sua senha: ')          #pega a senha

hash_object = hashlib.sha256()                      #gera o hash da senha

hash_object.update(usr.passwd.encode('utf-8'))

hash_value = hash_object.hexdigest()

usr.passwd = hash_value                             #salva o hash da senha no atributo passwd de usr

passwdcheck = input('confirme a sua senha: ')       #pega a senha do usuário novamente para confrimar

hash_object = hashlib.sha256()                      #gera o hash da senha redigitada

hash_object.update(passwdcheck.encode('utf-8'))

hash_value = hash_object.hexdigest()

passwdcheck = hash_value                            #salve o hash da senha redigitada


if passwdcheck == usr.passwd :                      #imprime "senha correta caso as senhas coincidam" 
    print('senha correta')

else:                                               #imprime "senha incorreta caso as senhas não coincidam
    print('senha incorreta')


print(f'usr.passwd = {usr.passwd}')

print(f'usr.name = {usr.name}')

print(f'usr.role = {usr.role}')